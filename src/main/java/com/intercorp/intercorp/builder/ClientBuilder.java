package com.intercorp.intercorp.builder;

import com.intercorp.intercorp.model.Client;
import com.intercorp.intercorp.model.response.ClientListResponse;
import com.intercorp.intercorp.model.response.ClientResponse;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientBuilder {

  public static ClientResponse builderEntityToResponse(List<Client> client) {

    return ClientResponse.builder()
            .average(calculateAgeAverage(client))
            .standardDeviation(calculateStandardDeviation(client))
            .build();
  }

  public static ClientListResponse builderToResponse(Client client) {
    return ClientListResponse.builder()
            .age(client.getAge())
            .birthday(client.getBirthday())
            .id(client.getId())
            .lastName(client.getLastName())
            .name(client.getName())
            .deathProbability(client.getBirthday().plusYears(getRandom(60, 70)).plusDays(getRandom(10, 20)).plusMonths(getRandom(1, 11)))
            .build();

  }

  private static double calculateAgeAverage(List<Client> client) {

    return (double) (client.stream()
            .map(Client::getAge)
            .mapToInt(Integer::intValue)
            .sum()) / 2;
  }

  public static double calculateStandardDeviation(List<Client> client) {
    //  Step 1: calcular la media.
    //  Step 2: Para cada punto de dato, encuentro el cuadrado de su distancia a la media.
    //  Step 3: Sumar los valores del Paso 2.
    //  Step 4: Dividir por la cantidad de puntos de datos.
    //  Step 5: Sa01car la raíz cuadrada.

    List<Integer> calculate = client.stream()
            .map(Client::getAge)
            .collect(Collectors.toList());

    // Step 1:
    double mean = mean(calculate);
    double temp = 0;

    for (int val : calculate) {
      // Step 2:
      double sqrtDiffToMean = Math.pow(val - mean, 2);

      // Step 3:
      temp += sqrtDiffToMean;
    }

    // Step 4:
    double meanOfDiffs = temp / (double) (calculate.size());

    // Step 5:
    return Math.sqrt(meanOfDiffs);
  }

  public static double mean(List<Integer> listAge) {
    double total = 0;
    for (int currentNum : listAge) {
      total += currentNum;
    }
    return total / listAge.size();
  }

  private static int getRandom(int min, int max) {

    return (int) (Math.random() * (max - min)) + min;
  }

}

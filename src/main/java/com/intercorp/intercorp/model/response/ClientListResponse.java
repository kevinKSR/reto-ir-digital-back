package com.intercorp.intercorp.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientListResponse {

  private String id;
  private String name;
  private String lastName;
  private Integer age;
  private LocalDate birthday;
  private LocalDate deathProbability;
}

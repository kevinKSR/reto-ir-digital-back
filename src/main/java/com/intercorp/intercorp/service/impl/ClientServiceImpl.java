package com.intercorp.intercorp.service.impl;

import com.intercorp.intercorp.builder.ClientBuilder;
import com.intercorp.intercorp.model.Client;
import com.intercorp.intercorp.model.response.ClientResponse;
import com.intercorp.intercorp.respository.ClientRepository;
import com.intercorp.intercorp.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

  private final ClientRepository clientRepository;

  @Override
  public Mono<Client> createClient(Client client) {
    return clientRepository.save(client);
  }

  @Override
  public Mono<ClientResponse> showDetailClients() {
    return clientRepository.findAll()
            .collectList()
            .map(ClientBuilder::builderEntityToResponse);
  }

  @Override
  public Flux<Client> listClient() {
    return clientRepository.findAll();
  }
}

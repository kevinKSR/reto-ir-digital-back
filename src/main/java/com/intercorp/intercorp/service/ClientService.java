package com.intercorp.intercorp.service;

import com.intercorp.intercorp.model.Client;
import com.intercorp.intercorp.model.response.ClientResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClientService {

  Mono<Client> createClient(Client client);

  Mono<ClientResponse> showDetailClients();

  Flux<Client> listClient();


}

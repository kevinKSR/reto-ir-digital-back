package com.intercorp.intercorp.controller;

import com.intercorp.intercorp.model.Client;
import com.intercorp.intercorp.model.response.ClientResponse;
import com.intercorp.intercorp.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/intercorp")
@AllArgsConstructor
public class ClientController {

  private final ClientService clientService;

  @PostMapping(value = "/create-client", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<Client> updateFactoryGetReport(@RequestBody Client client) {

    return clientService.createClient(client);
  }

  @GetMapping(value = "/client", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<ClientResponse> findClientById() {

    return clientService.showDetailClients();
  }

  @GetMapping(value = "/list-client", produces = MediaType.APPLICATION_JSON_VALUE)
  public Flux<Client> listClient() {

    return clientService.listClient();
  }


}

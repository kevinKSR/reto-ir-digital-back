package com.intercorp.intercorp.service;

import com.intercorp.intercorp.model.Client;
import com.intercorp.intercorp.respository.ClientRepository;
import com.intercorp.intercorp.service.impl.ClientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ClientServiceTest {

  @InjectMocks
  private ClientServiceImpl clientService;

  @Mock
  private ClientRepository clientRepository;

  @Test
  void createClient() {
    Client client = Client.builder()
            .id("1")
            .name("Kevin")
            .lastName("de Ortiz")
            .age(25)
            .birthday(LocalDate.of(1992, 2, 1))
            .build();

    when(clientRepository.save(any())).thenReturn(just(client));

    StepVerifier.create(clientService.createClient(client))
            .assertNext(client1 -> assertEquals("1", client1.getId()))
            .verifyComplete();

  }

  @Test
  void showDetailClients() {


    when(clientRepository.findAll()).thenReturn(Flux.just(Client.builder().age(25).build(), Client.builder().age(30).build()));

    StepVerifier.create(clientService.showDetailClients())
            .assertNext(clientResponse -> assertEquals(27.5, clientResponse.getAverage()))
            .verifyComplete();

  }

  @Test
  void listClient() {
    when(clientRepository.findAll()).thenReturn(Flux.just(Client.builder().id("1").age(25).build()));

    StepVerifier.create(clientService.listClient())
            .assertNext(client -> assertEquals("1", client.getId()))

            .verifyComplete();

  }
}
